function handleOpenModal(e) {
    $('#modal__overlay').removeClass('display-none');
    $('#modal__transaction').removeClass('display-none');

    const deviceName = String(e.target.dataset.devicename);
    const status = e.target.dataset.status;
    const keyword = e.target.dataset.keyword;

    let headerDeleteModalText = `
        Bạn chắc chắn muốn xóa thiết bị ${String(deviceName)}
    `;
    let deleteButtontemplate = `
        <input type="text" hidden name="deleteDevideId" value=${String(e.target.dataset.deviceid)} />
        ${(status !== '' && status !== '/')? `<input type="text" hidden name="status" value=${e.target.dataset.status} />` : `<div/>`}
        ${keyword !== '' ? `<input type="text" hidden name="keyword" value=${e.target.dataset.keyword} />` : `<div/>`}
        <button class="modal__btn --delete">Delete</button>
    `;
    
    $('#modal-transaction__header-text').html(headerDeleteModalText);
    $('#modal-device-delete').html(deleteButtontemplate);
}

function handleCloseModal(e) {
    if (e) {
        e.preventDefault();
    }
    $('#modal__overlay').addClass('display-none');
    $('#modal__transaction').addClass('display-none');
}
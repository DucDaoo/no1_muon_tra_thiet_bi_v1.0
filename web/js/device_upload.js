var inputFile = document.getElementById('avatar');
var inputText = document.getElementById('myFileName');

inputFile.addEventListener( 'change', showFileName );

function showFileName( event ) {
    var inputFile = event.srcElement;
    var fileName = inputFile.files[0].name;
    inputText.value = fileName;
}

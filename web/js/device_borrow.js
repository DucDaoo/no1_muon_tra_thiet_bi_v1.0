document.getElementById("submit-muon").addEventListener("click", function(e) {
    teacher_id = document.getElementById('teacher_id').value
    class_id = document.getElementById('class_id').value
    start_time_at = document.getElementById('start_time_at').value
    end_time_at = document.getElementById('end_time_at').value
    start_date_at = document.getElementById('start_at').value
    end_date_at = document.getElementById('end_at').value
    validate = true
    if (teacher_id < 1) {
      validate=false
      document.getElementById('error-teacher').innerHTML = "Vui lòng chọn giá viên mươn"
    }
    if (class_id < 1) {
      validate=false
      document.getElementById('error-class').innerHTML = "Vui lòng Chọn lớp mươn"
    }
    if (!start_date_at) {
      validate=false
      document.getElementById('error-date-start').innerHTML = "Vui lòng Chọn ngày mượn"
    }
  
    if (!end_date_at) {
      validate=false
      document.getElementById('error-date-end').innerHTML = "Vui lòng chọn ngày trả"
    }
    if (!start_time_at) {
      validate=false
      document.getElementById('error-time-start').innerHTML = "Vui lòng chọn giờ muộn"
    }
    if (!end_time_at) {
      validate=false
      document.getElementById('error-time-end').innerHTML = "Vui lòng chọn giờ trả"
    }
    if (validate) {
      if (start_date_at > end_date_at) {
        validate=false
        document.getElementById('error-date-end').innerHTML = "Thời gian trả phải lớn hơn thài gian mượn"
      }
      if (start_date_at == end_date_at) {
        if (start_time_at >= end_time_at) {
          validate=false
          document.getElementById('error-time-end').innerHTML = "Thời gian trả phải lớn hơn thài gian mượn"
        }
      }
    }
    if (!validate) {
      e.preventDefault()
    }
  });
  
  
  
  inputs = document.getElementsByClassName("input");
  
  for (i=0; i < inputs.length; i++) {
    inputs[i].addEventListener("change", function(e) {
      errors = document.getElementsByClassName('errors')
      for (j=0; j<   errors.length; j++) {
        errors[j].innerHTML = ""
      }
    })
  }
  
  
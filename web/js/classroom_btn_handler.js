function delete_classroom($id, $name) {
    let text = "Bạn chắc chắn muốn xóa phòng học " + $name + "?";
    var xhttp = new XMLHttpRequest();
    if (confirm(text) == true) {
        window.location.href = '../controller/classroom_delete.php?id=' + $id;
    }
}

function edit_classroom($id) {
    window.location.href = '../view/classroom_edit_input.php?id=' + $id;
}

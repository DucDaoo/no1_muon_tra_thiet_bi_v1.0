document.getElementById("submit").addEventListener("click", function(e) {
    building_id = document.getElementById('listbuildings').value
    class_name = document.getElementById('class_name').value
    mota = document.getElementById('mota').value
    avatar = document.getElementById('avatar').value
    validate = true
    if (!building_id) {
      validate=false
      document.getElementById('error-building').innerHTML = "Vui lòng chọn tòa nhà"
    }
    if (!class_name) {
      validate=false
      document.getElementById('error-class_name').innerHTML = "Vui lòng nhập tên lớp học"
    }
    if (!mota) {
      validate=false
      document.getElementById('error-mota').innerHTML = "Vui lòng nhập mô tả"
    }
    if (!avatar) {
      validate=false
      document.getElementById('error-avatar').innerHTML = "Vui lòng chọn avatar"
    }
    if (!validate) {
      e.preventDefault()
    }
  });
  inputs = document.getElementsByClassName("input");
  
  for (i=0; i < inputs.length; i++) {
    inputs[i].addEventListener("change", function(e) {
      errors = document.getElementsByClassName('errors')
      for (j=0; j<   errors.length; j++) {
        errors[j].innerHTML = ""
      }
    })
  }
  av = document.getElementById("avatar")
  if (av) {
    av.addEventListener("change", function(e) {
      file = e.target.files[0];
      if (!(file.type == "image/png" || file.type == "image/jpg" ||file.type == "image/jpeg" )) {
        document.getElementById('avatar').value = ''
        alert("Ảnh không đúng định dạng")
      }
    })
  }
  edit = document.getElementById("edit")
  if(edit){
  edit.addEventListener("click", function(e) {
  e.preventDefault();
  history.back()
  })
  }
  
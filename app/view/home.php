<?php
require_once '../common/db.php';
require_once '../controller/common.php';
$loginid = $_SESSION['loginid'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>HOME</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../web/css/home.css">

</head>

<body>
    <form>
        <div>
            <p>Tên login: <?php echo $loginid; ?></p>
        </div>
        <div>
            <p>Thời gian login: <?php date_default_timezone_set('Asia/Ho_Chi_Minh');
                                echo date("Y-m-d H:i") ?></p>
        </div>
        <div>
            <table>
                <tr>
                    <th>Phòng học</th>
                    <th>Giáo viên</th>
                    <th>Thiết bị</th>
                    <th>Mượn/trả thiết bị</th>
                </tr>
                <tr>
                    <td><a href="classroom_search.php">Tìm kiếm</a></td>
                    <td><a href="teacher_search.php">Tìm kiếm</a></td>
                    <td><a href="../controller/device_search.php">Tìm kiếm</a></td>
                    <td><a href="">Tìm kiếm</a></td>
                </tr>

                <tr>
                    <td><a href="classroom_add_input.php">Thêm mới</a></td>
                    <td><a href="teacher_add_input.php">Thêm mới</a></td>
                    <td><a href="device_add_input.php">Thêm mới</a></td>
                    <td><a href="../controller/specification_search.php">Tìm kiếm nâng cao</a></td>
                </tr>
                
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="../controller/device_return.php">Trả thiết bị</a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="../controller/device_loan_history.php">Lịch sử mượn thiết bị</a></td>
                </tr>
            </table>
        </div>
        <div><br><br><br>
            <h4><a style="text-decoration: none" href="../controller/reset_password.php">Reset password</a> | <a style="text-decoration: none"
                    href="../controller/logout.php">Logout</a> </h4>
        </div>
        <form>
</body>

</html>
<?php 
    require_once "../controller/common.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Teachers Searching Screen</title>
    <link rel="stylesheet" href="../../web/css/teacher_search.css">
</head>
<body>
<div class="container">
    <form action="../controller/teacher_search.php" method="GET">
    <div>
        <div class="center">
        <span class="keyword_content">Chuyên ngành</span>
        <select class="selbox" name="specialized">
            <option value="000"></option>
            <option value="001">Khoa học máy tính</option>
            <option value="002">Khoa học dữ liệu</option>
            <option value="003">Hải dương học</option>
        </select>
        </div>

        <div class="center" style="margin-top: 20px">
        <span class="keyword_content">Từ khóa</span>
        <input type="text" name="keyword" class="searchbox"></input>
        </div>
        <div class="subsp">
        <input type="submit" value="Tìm kiếm" class="searchbt"; ></input>
        </div>
    </div>
    </form>

    <?php if (isset($keyword)) {
    echo '        
    <div>
        <div class="total">
            <span>Số giáo viên tìm thấy: '; echo (count($list_teacher));echo '</span>
        </div>
        <table style="margin-left: 100px; margin-right: 100px">
            <tr>
                <th style="width: 5%">No</th>
                <th style="width: 35%">Tên giáo viên</th>
                <th style="width: 20%">Khoa</th>
                <th style="width: 39%">Mô tả chi tiết</th>
                <th style="width: 1%">Action</th>
            </tr> ';
                for ($i=0;$i<=count($list_teacher)-1;$i++) {
                    echo '
                        <tr>
                            <td>'.($i+1).'</td>
                            <td>'.$list_teacher[$i]['name'].'</td>
                            <td>';  if ($list_teacher[$i]['specialized'] == "001") { echo "Khoa học máy tính"; }
                                    if ($list_teacher[$i]['specialized'] == "002") { echo "Khoa học dữ liệu"; }
                                    if ($list_teacher[$i]['specialized'] == "003") { echo "Hải dương học"; }
                            echo '</td>
                            <td>'.$list_teacher[$i]['description'].'</td>
                            <td>
                            <form method="POST" action="../controller/teacher_search.php">
                            <button class="btn" name="del" onclick=\'javascript: return confirm("Bạn chắc chắn muốn xóa giáo viên '.$list_teacher[$i]['name'].' ?");\'value = ';echo $list_teacher[$i]['id'];echo">Xóa</button></form></td>";
                            echo'<td><button onclick=\'javascript: location.href="../view/teacher_edit_input.php?teacherId=' . $list_teacher[$i]["id"] . '";\' class="btn">Sửa</button>
                            </td>
                        </tr>
                    ';
                }
        echo '</table>';
    echo '</div>';
    } 
    unset($_SESSION["id_teacher"],$_SESSION["name"],$_SESSION["avatar"],$_SESSION["specialized"],$_SESSION["degree"],$_SESSION["motathem"]);?>
</div>
</body>
</html>

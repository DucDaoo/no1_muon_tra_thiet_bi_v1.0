<?php
require_once "../common/db.php";
require_once "../common/define.php";
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xác nhận thông tin</title>
    <link rel="stylesheet" href="../../web/css/teacher_add_confirm.css">
</head>
<body>
    <div class="container">
        <div class="form-input-line">
            <label class="input-label">
                Họ và tên
            </label>
            <input type="text" name="name" class="form-info" value="<?php echo $_SESSION["name"];?>" readonly>
        </div>
        <div class="form-input-line">
            <label class="input-label">
                Chuyên ngành
            </label>
            <input type="text" name="specialized" class="form-info" value="<?php echo _SPECIALIZED[$_SESSION["specialized"]];?>" readonly>
        </div>
        <div class="form-input-line">
            <label class="input-label">
                Học vị
            </label>
            <input type="text" name="degree" class="form-info" value="<?php echo _DEGREES[$_SESSION["degree"]];?>" readonly>
        </div>
        <div class="form-input-line" >
            <label class="input-label">
                Avatar
            </label>
            <div class="avatar">
                <?php
                    $dir = "../../web/avatar/teacher/";
                    $img_dir = $dir . basename($_SESSION['avatar']);
                    echo '<img src="';
                    echo $img_dir;
                    echo '" alt="avatar" style="width:100px;height:100px;">';
                ?>
            </div>
        </div>
        <div class="form-input-line">
            <label class="input-label">
                Mô tả thêm
            </label>
            <textarea name="description" id="description" style="width: 350px; height: 100px; background-color: #d6d8d9;" readonly><?php echo $_SESSION["description"];?></textarea>
        </div>
        <div class="button-line">
            <form action="teacher_add_input.php" method="get">
                <input type="submit" class="submit-button" name= "fix" value="Sửa lại">
            </form>
            <form action="../../app/controller/teacher_add.php" method="post">
                <input class="submit-button" type="submit" name ="submit" value="Xác nhận">
            </form>
        </div>
    </div>
</body>
</html>
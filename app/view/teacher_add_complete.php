<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Complete</title>
    <link rel="stylesheet" href="../../web/css/teacher_add_complete.css">
</head>
<body>
    <div class="container">
        <div class="noti">
            Bạn đã đăng ký thành công giáo viên.
        </div>
        <div class="backtohome">
            <a href="home.php" target="_self" style="color: black; ">Trở về trang chủ</a>
        </div>
    </div>
</body>
</html>
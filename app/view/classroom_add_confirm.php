<?php
    require_once "../../app/controller/common.php";
    require_once "../common/define.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../../web/css/classroom_add.css">
</head>
<body>
    <div class="form_muon">
        <p class="caption">Xác nhận đăng ký lớp học</p>
        <div class="form-group">
            <div class="label-form"><label>Tên lớp học </label></div>
            <?php
            echo $class_name;
            ?>
            <br>
            <div class="label-form"><label></label></div>
        </div>
        <div class="form-group">
            <div class="label-form"><label>Tòa nhà</label></div>
            <?php
                echo _BUILDING[$_POST["building_id"]];
            ?>
            <br>
            <div class="label-form"><label></label></div>
        </div>
        <div class="form-group">
            <div class="label-form"><label>Mô tả chi tiết </label></div>
            <?php
            echo $mota;
            ?>
            <div class="label-form"><label></label></div>
        </div>
        <div class="form-group">
            <div class="label-form"><label>Avatar </label></div>
            <image style="width:200px;height:100px;" src="../../web/avatar/tmp/<?php echo $avatar['name'];?>"></image>
            <div class="label-form"><label></label></div>
        </div>

        <form method="POST" action="../controller/classroom_add.php">
            <input type="hidden" name="type" value="1" />
            <input type="hidden" name="class_name" value="<?php echo $class_name;?>" />
            <input type="hidden" name="building_id" value="<?php echo $building_id;?>" />
            <input type="hidden" name="mota" value="<?php echo $mota;?>" />
            <input type="hidden" name="avatar" value="<?php echo $avatar['name'];?>" />

        <div class="btn-group">
            <button class="btn" id ='edit' name="type" value="2" >Sửa</button>
            <button class="btn"  id='save' name="save" type="submit">Lưu</button>
        </div>
        </form>
</div>

</body>
</html>
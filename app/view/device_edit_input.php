<?php

// $id_equipment=$_SESSION['id'];
$id_equipment = $_GET['deviceId'];//
require_once '../common/db.php';
try {
    $con_PDO = new PDO("mysql:host=$severName;dbname=$myDB", $username, $password);
} catch (Exception $ex) {
    echo ($ex->getMessage());
}
$avatar = '';
?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../../web/css/device_edit_input.css">
    <title>Add</title>
   
</head>
<?php
require_once '../controller/device_edit.php';
?>

<body>

    <form method="post" action="" enctype="multipart/form-data">
        <fieldset>
            <?php
            $sql_equipment = "SELECT * FROM devices WHERE id='$id_equipment';";
            $equipment = $con_PDO->prepare($sql_equipment);
            $equipment->execute();
            foreach ($equipment as $row) {
                $avatar = $row['avatar'];
            ?>
                <div class="fullname">
                    <label for="fullname">Tên thiết bị</label>
                    <input id="fullname" type="text" name="fullname" value="<?php echo $row[1] ?>"></input>

                </div>
                <span class="error"><?php echo $devices_null; ?> </span>

                <br>
                <div class="motathem">
                    <label class="motathem" for="motathem">Mô tả chi tiết</label>
                    <br>
                    <textarea id="motathem_text" type="textarea" name="motathem" rows="5" cols="40">
                        <?php echo trim($row['description']) ?>
                    </textarea>

                </div>
                <span class="error"><?php echo $error_motathem; ?> </span>
                <div class="avatar">
                    <label for="avatar">Avatar</label>
                    <br>

                    <img id="img_avatar" src="<?php echo ("../../web/avatar/" . $row['avatar']); ?>">
                </div>
                <input id="ava_text" type="file" name="avatar" value="upload" />
                
                <span class="error"><?php echo $error_avatar; ?> </span>

                <div class="submit">
                    <button type="submit" id="submit" name="submit">Xác nhận</button>
                </div>
            <?php } ?>

        </fieldset>
    </form>


</body>

</html>
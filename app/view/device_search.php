<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../web/css/device_search.css">
    <link rel="stylesheet" href="../../web/css/modal.css">
    <title>History Search Devices</title>
</head>

<body>
    <div class="container">
        <form action="../controller/device_search.php" method="GET">
            <div class="search">
                <div class="row">
                    <span class="keyword_content">Từ khóa</span>
                    <input type="text" name="keyword" value="<?php echo $keyword ?>">
                </div>
                <div class="row">
                    <span class="keyword_content">Tình trạng</span>
                    <select name="status" value="<?php echo $_GET['status'] ?>">
                        <?php if ($_GET['status'] === "") { ?>
                            <option selected value=""></option>
                        <?php } else { ?>
                            <option value=""></option>
                        <?php } ?>

                        <?php if ($_GET['status'] === "empty") { ?>
                            <option selected value="empty">Rỗng</option>
                        <?php } else { ?>
                            <option value="empty">Rỗng</option>
                        <?php } ?>

                        <?php if ($_GET['status'] === "borrow") { ?>
                            <option selected value="borrow">Đang mượn</option>
                        <?php } else { ?>
                            <option value="borrow">Đang mượn</option>
                        <?php } ?>

                        <?php if ($_GET['status'] === "ready") { ?>
                            <option selected value="ready">Đang rảnh</option>
                        <?php } else { ?>
                            <option value="ready">Đang rảnh</option>
                        <?php } ?>
                    </select>
                </div>
                <div class="submit">
                    <input type="submit" id="finding" value="Tìm kiếm">
                </div>
            </div>
        </form>

        <div class="result">
            <div class="row">
                <span class="">Thiết bị tìm thấy: <?php echo (count($list_devices)); ?></span>
            </div>
            <table>
                <thead>
                    <tr>
                        <th style="width: 5%">No</th>
                        <th style="width: 20%">Tên thiết bị</th>
                        <th style="width: 35%">Trạng thái</th>
                        <th style="width: 20%">Action</th>
                    </tr>
                </thead>
                <tbody>
                <tbody>
                    <?php
                    for ($i = 0; $i <= count($list_devices) - 1; $i++) {
                        $deviceId = $list_devices[$i]['id'];
                        $deviceStatus = getDeviceStatusKeyById($deviceId);
                        if ($deviceStatus === "ready") {
                            echo '
                                    <tr>
                                        <td>' . ($i + 1) . '</td>
                                        <td>' . $list_devices[$i]['name'] . '</td>
                                        <td>' . getDeviceStatusText($deviceStatus) . '</td>
                                        <td style="display: flex">
                                            <button 
                                            data-deviceid=' . $list_devices[$i]["id"] . ' 
                                            data-status="' . $status . '"
                                            data-devicename=' . $list_devices[$i]["name"] . '
                                            data-keyword="' . $keyword . '" 
                                            class="btn btn-action" 
                                            onclick="handleOpenModal(event)
                                            ">Xóa</button>
                                            <a href="../view/device_edit_input.php?deviceId=' . $list_devices[$i]["id"] . '" style="margin-left: 6px; color: black;" class="btn btn-action flex-center">Sửa</a>
                                            </td>
                                    </tr>
                                ';
                        } else {
                            echo '
                                    <tr>
                                        <td>' . ($i + 1) . '</td>
                                        <td>' . $list_devices[$i]['name'] . '</td>
                                        <td>' . getDeviceStatusText($deviceStatus) . '</td>
                                        <td></td>
                                    </tr>
                                ';
                        }
                    }
                    ?>
                </tbody>
                </tbody>
            </table>
        </div>

        <!-- Modal -->
        <div id="modal__overlay" class="display-none"></div>
        <div id="modal__transaction" class="display-none">
            <div class="modal__detail-transaction">
                <div class="modal-transaction__header">
                    <div id="modal-transaction__header-text" class="modal-transaction__header-text">

                    </div>
                    <div class="modal-transaction__header-close" onclick="handleCloseModal()">x</div>
                </div>
                <div class="modal-transaction__form">
                    <div id="modal__transaction-form" class="modal-transaction__form-container">
                    </div>

                    <div class="modal-transaction__control">
                        <button class="modal__btn --close" onclick="handleCloseModal(event)">Cancel</button>
                        <form id="modal-device-delete" method="POST" action="../controller/device_search.php">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<!-- <script type='text/javascript'>
    document.body.onclick(function(e) {
        console.log(e.target.dataset.);
    });
</script> -->
<script src="../../web/js/device_search.js"></script>

</html>
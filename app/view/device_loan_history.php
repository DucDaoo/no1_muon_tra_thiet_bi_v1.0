<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../web/css/device_loan_history.css">
    <title>History Borrow Devices</title>
</head>
<body>
    <div class="container">

        <form action="../controller/device_loan_history.php" method="post">
            <div class="search">
                <div class="row">
                    <span>Thiết bị</span>
                    <input type="text" name="name_device" value="<?php echo (isset($name_device))?$name_device:'';?>">
                </div>
                <div class="row">
                    <span>Giáo viên</span>
                    <select name="id_teacher" id="">
                        <option value=""></option>
                        <?php
                            
                            foreach ($list_all_teacher as $key => $value) {
                                $selected = '';
                                if (isset($id_teacher) && $id_teacher == $value['id'])
                                {
                                    $selected = 'selected="selected"';
                                }
                                echo '<option value="'.$value['id'].'"'.$selected.'>'.$value['name'].'</option>';  
                            }
                        ?>
                    </select>
                </div>
                <div class="submit">
                    <input type="submit" id="finding" value="Tìm kiếm">
                </div>
            </div>

            <div class="result">
                <div class="row">
                    <span class="">Số lần thiết bị tìm thấy: <?php echo (count($list_devices)); ?></span>
                    
                </div>
                <table>
                    <thead>
                        <tr>
                            <th style="width: 5%">STT</th>
                            <th style="width: 20%">Tên thiết bị</th>
                            <th style="width: 35%">Thời gian dự kiến mượn</th>
                            <th style="width: 20%">Thời điểm trả</th>
                            <th style="width: 20%">Giáo viên mượn</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        for ($i = 0; $i <= count($list_devices)-1; $i++) {
                            $list_devices[$i]['device_transactions_start_transaction_plan'] = formatDate($list_devices[$i]['device_transactions_start_transaction_plan']);
                            $list_devices[$i]['device_transactions_end_transaction_plan'] = formatDate($list_devices[$i]['device_transactions_end_transaction_plan']);
                            $list_devices[$i]['device_transactions_returned_date'] = formatDate($list_devices[$i]['device_transactions_returned_date']);

                            echo '
                                <tr>
                                    <td>'.($i+1).'</td>
                                    <td>'.$list_devices[$i]['devices_name'].'</td>
                                    <td>'.$list_devices[$i]['device_transactions_start_transaction_plan'].' ~ '.$list_devices[$i]['device_transactions_end_transaction_plan'].'</td>
                                    <td>'.$list_devices[$i]['device_transactions_returned_date'].'</td>
                                    <td>'.$list_devices[$i]['teachers_name'].'</td>
                                </tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </form> 
    </div>
</body>
</html>
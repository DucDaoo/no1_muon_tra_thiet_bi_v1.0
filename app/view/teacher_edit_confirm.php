<?php
    require_once "../../app/controller/common.php";
    require_once "../model/teacher.php";
?>

<html>
    <head>
        <title>Sửa thông tin giáo viên</title>
        <link rel="stylesheet" href="../../web/css/teacher_edit_confirm.css">
    </head>

    <body>
        <form class='body' method='POST' action="../../app/controller/teacher_edit.php">
            <br>
            <div class='left'>
                <label class='title'>Tên</label>
                <label class='break2'></label>
                <label name='name' class="input"> <?php echo $_SESSION['name'] ?> </label> <br>
            </div>
            <div class='break'></div>
            <div class='left'>
                <label class='title'>Chuyên ngành</label>
                <label class='break2'></label>
                <label name='name'class="input"> <?php echo $_SESSION['specialized'] ?></label> <br>
            </div>
            <div class='break'></div>
            <div class='left'>
                <label class='title'>Học vị</label>
                <label class='break2'></label>

                <label name='name'class="input"> <?php echo $_SESSION['degree']?> </label> <br>
            </div>
            <div class='break'></div>
            <div class="left">
                <label for="avatar" class="title">Ảnh</label>
                <label class='break2'></label>
                <img id="img_avatar" src="<?php
                            echo "../../web/avatar/".$_SESSION["avatar"]; ?>">
            </div>

            <div class='break3'></div>

            <div class="left">
                <label class="title_hidden">Avatar</label>
            </div>

            <div class="left">
                <label class="title" for="description">Mô tả thêm</label>
                <label class='break2'></label>
                <label class="textarea" type="textarea" name="description" rows="3" cols="40">
                <?php echo $_SESSION['description'] ?>
                </label>
            </div>

            <div class='break3'></div>

            <div class="left">
                <input class='signup2' type='submit' name='buttonSubmit'value=' Sửa lại'>
                <label class='break2'></label>
                <input class='signup2' type='submit' name='buttonSubmit'value='Đăng ký'>
            </div>

            <div class='break'></div>
        </form>
    </body>

</html> 

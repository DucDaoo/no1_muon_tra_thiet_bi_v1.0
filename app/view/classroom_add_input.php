<?php
    require_once  '../model/classroom.php';
    require_once "../common/define.php";
    require_once "../../app/controller/common.php";
?>

<html>

<head>
    <title>GK</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../../web/css/classroom_add.css">
    <script>

    </script>
</head>
<body>
<!-- <form name="frmLogin" action="b.php" method="get"> -->
    <form class="form_muon" action="../controller/classroom_add.php" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="type" value="0" />
        <p class="caption">Đăng kí lớp học</p>
        <div class="form-group">
            <div class="label-form"><label>Tên phòng học </label></div>
            <input class="input form-control" name="class_name" id="class_name"/>
            <div class="label-form"><label></label></div>
            <span class="errors" id="error-class_name"></span>
        </div>
        <div class="form-group">
            <div class="label-form"><label>Tòa nhà</label></div>
            <select name="building_id" id="building_id" class="input form-control">
                <option>--Tòa nhà--</option>
                <?php
                    foreach(_BUILDING as $key => $value){ ?>
                            <option value="<?=$key?>"><?=$value?></option>
                    <?php }
                ?>
            </select>
            <br>
            <div class="label-form"><label></label></div>
            <span class="errors" id="error-building"></span>
        </div>

        <div class="form-group">
            <div class="label-form"><label>Mô tả chi tiết </label></div>
            <textarea class="input form-control" id="mota" name="mota"></textarea>
            <div class="label-form"><label></label></div>
            <span class="errors" id="error-mota"></span>
        </div>
        <div class="form-group">
            <div class="label-form"><label>Avatar </label></div>
            <input type="file"  name="avatar" class="input" id="avatar" />
            <div class="label-form"><label></label></div>
            <br>
            <div class="label-form"><label></label></div>

            <span class="errors" id="error-avatar"></span>
        </div>
        <button class="btn" id="submit" type="submit">Xác Nhận</button>


    </form>
</body>

<script src="../../web/js/classroom_add.js"></script>

</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../web/css/reset_password.css">
    <title>RESET PASSWORD</title>
</head>
<body>
    <form action="../controller/reset_password.php" method="POST">
        <div class="errors">
            <span>
                <?php echo $msg_error; ?>
            </span>
        </div>

        <table>
            <tr>
                <th>NO</th>
                <th>Tên người dùng</th>
                <th>Mật khẩu mới</th>
                <th>Action</th>
            </tr>
            
            <?php
                for($i=0; $i<count($list_info); $i++){
            ?>
                <tr>
                    <td><?php echo $i+1;?></td>
                    <td><input class="input_readonly" name="id_reset" value="<?php echo $list_info[$i]['login_id']?>" readonly></td>
                    <td><input type="text" name="password_new"></td>
                    <td><input type="submit" name="btn_reset" value="Reset"></td>
                </tr>
            <?php }?>
        </table>
        <div><br><br><a href="../view/home.php">Trở về trang chủ</a></div>
        </form>
    
</body>
</html>
<?php
require_once "../common/db.php";
require_once "../common/define.php";
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thêm mới giáo viên</title>
    <link rel="stylesheet" href="../../web/css/teacher_add_input.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</head>

<body>
    
<?php
    $isValid = true;
    $error_name = "";
    $error_specialized = "";
    $error_degree = "";
    $error_avatar = "";
    $error_description = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        function inputHandling($data) {
            $data = trim($data);
            $data = stripslashes($data);
            return $data;
        }
        if (empty(inputHandling($_POST["name"]))) {
            $error_name = "Hãy nhập tên giáo viên.";
            $isValid = false;
        } 
        if (strlen(inputHandling($_POST["name"])) > 100) {
            $error_name = "Không nhập tên quá 100 ký tự.";
            $isValid = false;
        }
        if (empty($_POST["specialized"])) {
            $error_specialized = "Hãy chọn chuyên ngành.";
            $isValid = false;
        }
        if (empty($_POST["degree"])) {
            $error_degree = "Hãy chọn bằng cấp.";
            $isValid = false;
        }
        if (empty($_FILES['avatar']['name'])) {
            $error_avatar = "Hãy chọn avatar";
            $isValid = false;
        }
        if (empty(inputHandling($_POST["description"]))) {
            $error_des = "Hãy nhập mô tả chi tiết";
            $isValid = false;
        } 
        else if (strlen(inputHandling($_POST["description"])) > 1000) {
            $error_des = "Không nhập mô tả chi tiết quá 1000 ký tự";
            $isValid = false;
        }
        if ($isValid) {
            $_SESSION = $_POST;
            $_SESSION["avatar"] = $_FILES['avatar']['name'];
            $target_path = "../../web/avatar/teacher/";
            if (!file_exists($target_path)) {
                mkdir($target_path);
            }
            $target_path = $target_path . basename($_FILES['avatar']['name']);

            if(copy($_FILES['avatar']['tmp_name'], $target_path)) {
                header('location: teacher_add_confirm.php');
                #unset($_SERVER["REQUEST_METHOD"]);
            }
            exit();
        }
    }
?>

    <div class="container">
    <form action="" method="POST" enctype="multipart/form-data">

<?php 
    if (!$isValid) { 
        echo '<div class="error">';
        echo '<label>';
        echo $error_name;
        echo '</label>';
        echo '</div>';
    } 
?>
    <div class="form-input-line">
        <label class="input-label form-group required control-label">
            Họ và tên
        </label>
        <input 
        type="text" 
        name="name" 
        style="width: 192px; background-color: #d6d8d9; padding: 5px 0px 5px 5px;" 
        value="<?php 
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            echo isset($_POST["name"]) ? $_POST["name"] : ''; 
        } else {
            echo isset($_SESSION['name']) ? $_SESSION['name'] : '';
        }
        ?>">
    </div>

<?php 
    if (!$isValid) { 
    echo '<div class="error">';
    echo '<label>';
    echo $error_specialized;
    echo '</label>';
    echo '</div>';
    } 
?>

    <div class="form-input-line">
        <label class="input-label form-group required control-label">
            Chuyên ngành
        </label>
        <select name="specialized" id="specialized" style="width: 200px; background-color: #d6d8d9; padding: 5px 0px 5px 5px;">
            <?php
                foreach (_SPECIALIZED as $key => $value) { ?>
                	<option value="<?=$key?>"><?=$value?></option>
                <?php }
            ?>
        </select>
    </div>

    <script type="text/javascript">
        document.getElementById('specialized').value = "<?php
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            echo $_POST['specialized'];
        } else {
            echo $_SESSION['specialized'];
        }
        ?>";
    </script>

<?php 
    if (!$isValid) { 
        echo '<div class="error">';
        echo '<label>';
        echo $error_degree;
        echo '</label>';
        echo '</div>';
    } 
?>

    <div class="form-input-line">
        <label class="input-label form-group required control-label">
            Học vị
        </label>
        <select name="degree" id="degree" style="width: 200px; background-color: #d6d8d9; padding: 5px 0px 5px 5px;">
            <?php
                foreach (_DEGREES as $key => $value) { ?>
                	<option value="<?=$key?>"><?=$value?></option>
                <?php }
            ?>
        </select>
    </div>

    <script type="text/javascript">
        document.getElementById('degree').value = "<?php
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            echo $_POST['degree'];
        } else {
            echo $_SESSION['degree'];
        }
        ?>";
    </script>

<?php 
    if (!$isValid) { 
        echo '<div class="error">';
        echo '<label>';
        echo $error_avatar;
        echo '</label>';
        echo '</div>';
    } 
?>
    <div class="form-input-line" >
        <label class="input-label form-group required control-label">
            Avatar
        </label>
        <div>   
            <div class="form_item_file" id="browse">
                <div class="form_item_filename" id="filename"></div>
                <div class="form_item_button">
                    <label class="form_item_upload" for="avatar">Chọn tệp</label>
                    <input type="file" id="avatar" name="avatar" accept="image/*"/>
                </div>
            </div>
        </div>
    </div>
        
<?php 
    if (!$isValid) { 
        echo '<div class="error">';
        echo '<label>';
        echo $error_description;
        echo '</label>';
        echo '</div>';
    } 
?>
    <div class="form-input-line">
        <label class="input-label form-group required control-label">
            Mô tả thêm
        </label>
        <textarea name="description" id="description" style="width: 350px; height: 100px; background-color: #d6d8d9;"></textarea>
    </div>

    <script type="text/javascript">
        document.getElementById('description').value = "<?php
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            echo $_POST['description'];
        } else {
            echo $_SESSION['description'];
        }
        ?>";
    </script>

    <div class="form-input-line" style="align-items: center; display: flex; justify-content: center;">
        <input class="submit-button" type="submit" value="Xác nhận">
    </div>

</form>
</div>

</body>
    <script>
    $('#avatar').change(function() {
        var i = $(this).prev('label').clone();
        var file = $('#avatar')[0].files[0].name;
        document.getElementById("filename").innerHTML = file;
        document.getElementById("avatar").src = window.URL.createObjectURL(this.files[0]);
    });
    </script>
</html>

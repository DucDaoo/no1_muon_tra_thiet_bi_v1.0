<?php
require_once '../controller/common.php'; 
require_once '../model/classroom.php';
require_once '../common/define.php';
$building_id = '';
$keyword_name = '';
if (isset($_SESSION['classrooms'])) {
    $classrooms = $_SESSION['classrooms'];
} else {
    $classrooms = [];
}
if (isset($_SESSION['building_id'])) {
    $building_id = $_SESSION['building_id'];
}
if (isset($_SESSION['keyword_name'])) {
    $keyword_name = $_SESSION['keyword_name'];
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../web/css/classroom_search.css">
    <script src="../../web/js/classroom_btn_handler.js"></script>
    <title>Classroom Search</title>
</head>
<body>
    <form action="../controller/classroom_search.php" method="get">
        
        <div class='frame'>
            <div class='search'>
                <div class="row">
                <span class="label_box">Tòa nhà</span>
                        <select name="building_id" id="">
                            <option value=""></option>
                            <?php 
                                foreach (_BUILDING as $key => $value) {
                                    if ($building_id == $key) {
                                        echo "<option selected value='$value'> $value </option>";
                                    } else {
                                        echo "<option value='$value'> $value </option>";  
                                    }
                                }
                            ?>
                        </select>
                </div>
                <div class="row">
                    <span class="label_box">Từ khóa</span>
                    <input class = 'input_box' type="text" name="keyword_name" value="<?php echo $keyword_name; ?>">
                </div>
                <div class="submit">
                        <input type="submit" id="finding" value="Tìm kiếm">
                </div>
            </div>

            <div class="result">
                    <div class="row">
                        <span class="label_box1">Số phòng học tìm thấy: <?php echo count($classrooms); ?></span>
                    </div>
                    <br>

                    <div class="scroll_table">
                        <table>
                            <thead>
                                <tr>
                                    <th style="width: 5px">No</th>
                                    <th style="width: 150px">Tên phòng học</th>
                                    <th style="width: 150px">Tòa nhà</th>
                                    <th style="width: 250px">Mô tả chi tiết</th>
                                    <th style="width: 150px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                $record_number = 0;
                                foreach($classrooms as $row) {
                                    $record_number++;
                                    $id = $row['id'];
                                    $name = $row['name'];
                                    $building = $row['building'];
                                    $description = $row['description'];
                            
                                    echo "<tr>
                                            <td>$record_number</td>
                                            <td>$name</td>
                                            <td>$building</td>
                                            <td>$description</td>
                                            <td>
                                                <div style='display: flex;'>
                                                <div class='button_row left_button' onclick='delete_classroom($id, \"" . $name . "\")'>Xóa</div>
                                                <div class='button_row' onclick='edit_classroom($id)'>Sửa</div>
                                                </div>
                                            </td>
                                        </tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </form>
    
</body>
</html>
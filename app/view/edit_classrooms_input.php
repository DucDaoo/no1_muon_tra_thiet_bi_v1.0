<?php 
    //$id_equipment= $_GET['id'];
    $id_equipment= 1;
    require_once '../common/db.php';
    try{
        $con_PDO = new PDO("mysql:host=$severName;dbname=$myDB", $username, $password);
    }
    catch(EXCeption $ex){
        echo($ex -> getMessage());
    }
    $avatar='';
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet"  href="../../web/css/edit_classrooms.css">
<title>INPUT</title>

</head>
<?php 
    require_once '../controller/edit_classrooms.php';
    require_once '../common/define.php';
?>
<body>
    <form method="post" action="" enctype="multipart/form-data">
        <fieldset>
        <?php
            $sql_equipment = "SELECT * FROM classrooms WHERE id= $id_equipment";
            $equipment = $con_PDO->prepare($sql_equipment);
            $equipment->execute();
            foreach ($equipment as $row) {
            $avatar=$row['avatar'];
            ?>
                <div class="name">
                    <label for="name">Tên Phòng học</label>
                    <input id = "name" type="text" name="name" value="<?php echo $row['name']?>"></input>  
                </div>
                <span class="error"><?php echo $error_message; ?> </span> 

                <div class="building">
                    <label for="building">Tòa nhà</label>
                    <select name="building" id="buiding">
                            <option value=""></option>
                            <?php 
                                foreach (BUILDING as $key => $value) {
                                    if ($building == $key) {
                                        echo "<option selected value='$key'> $value </option>";
                                    } else {
                                        echo "<option value='$key'> $value </option>";  
                                    }
                                }
                            ?>
                        </select>
                        
                          
                </div>
                <span class="error"><?php echo $error_message; ?> </span> 

                <div class="description">
                    <label class="description" for="description">Mô tả chi tiết</label>
                    <textarea id="motathem_text" type="textarea" name="description" rows="5" cols="40" >
                        <?php echo trim($row['description'])?>
                    </textarea>

                </div>
                <span class="error"><?php echo $error_motathem; ?> </span> 
                <div class="avatar">
                    <label for="avatar">Avatar</label>

                    <img id="img_avatar" src="<?php echo ("../../web/avatar/" .$row['avatar'] ); ?>">
                    <!-- <input id = "ava" type="text" name="ava" value="<?php echo $avatar; ?>"></input>  -->
                </div>
                <input id = "ava_text" type="file" name="avatar" value="upload"/>
                <span class="error"><?php echo $error_avatar; ?> </span> 

                <div class="submit">
                    <button type="submit" id = "submit" name="submit">Xác nhận</button>
                </div>
                <?php }?>

        </fieldset>
    </form>


</body>
</html> 

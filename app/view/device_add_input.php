<?php

require_once "../../app/controller/common.php";// điều kiện nếu chưa login thì xuất ra màn hình login
$error = array();
if (isset($_POST['submit'])) {
    if (empty($_POST['name'])) {
        $error['name'] = 'Hãy nhập tên thiết bị. <br>';
    }
    if (empty($_POST['description'])) {
        $error['description'] = 'Hãy nhập mô tả chi tiết. Không nhập quá 1000 kí tự. <br>';
    }
    if (empty($_FILES["avatar"]["name"])) {
        $error['avatar'] = 'Hãy chọn Avatar. <br>';
    }
    if (empty($error)){
        $uploads_dir = __DIR__ . '/../../web/avatar/tmp'; 
        if (!file_exists($uploads_dir)) { 
            echo "Upload path not found";
            exit();
        }
        
        $tmp_name = $_FILES["avatar"]["tmp_name"]; 

        $avatarname = basename($_FILES["avatar"]["name"]); 

        if (!move_uploaded_file($tmp_name, "$uploads_dir/$avatarname")) { 
            echo "Fail";
        }
        // session_start();
        $_SESSION['name'] = $_POST['name'];
        $_SESSION['description'] = $_POST['description'];
        $_SESSION['avatar'] = $_FILES["avatar"]["name"];
       
        header("location: device_add_confirm.php");
        
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Input</title>
    <link rel="stylesheet" href="../../web/css/device_add.css">
    <script type="text/javascript" src="../../web/js/device_upload.js"></script> 
</head>
<body>
    <form action="" method="POST" enctype="multipart/form-data" class="form-device">

        <div id="error" class ="error">
            <span>
                <?php
                    if(!empty($error)) {
                        foreach ($error as $er) {
                            echo $er;
                        }
                    }
                ?>
            </span>
        </div>

        <div class="row"> 
            <div class="col col-1"> 
                <label for="" class="label-name">Tên thiết bị</label>
            </div>
            <div class="col col-2"> 
                <input type="text" name="name" id="name" class="input-name" 
                    value="<?php 
                                if ($_SERVER['REQUEST_METHOD'] == "POST") {
                                    echo isset($_POST["name"]) ? $_POST["name"] : ''; 
                                } else {
                                    echo isset($_SESSION['name']) ? $_SESSION['name'] : '';
                                }
                            ?>">
            </div>
        </div>

        <div class="row">
            <div class="col col-1"> 
                <label for="" class="label-description">Mô tả chi tiết</label>
            </div>
            <div class="col col-2"> 
                <textarea name="description" id="description" cols="30" rows="10" class="textarea-description"><?php 
                        if ($_SERVER['REQUEST_METHOD'] == "POST") {
                            echo isset($_POST["description"]) ? $_POST["description"] : ''; 
                        } else {
                            echo isset($_SESSION['description']) ? $_SESSION['description'] : '';
                        }
                    ?></textarea> 
            </div>
        </div>

        <div class="row">
            <div class="col col-1"> 
                <label for="" class="label-avt">Avatar</label>
            </div>
            <div class="col col-2"> 
                <input type="text" id="myFileName" readonly>
                <label for="avatar" style="border: 3px solid #ccc;">Browse</label>
                <input type="file" id="avatar" name="avatar" accept=".jpg, .jpeg, .png" style="display:none" >
            </div>
        </div>

        <div class="row" style="text-align: center;">
                <input type="submit"  name="submit" value="Xác Nhận" class="input-btn">
        </div>
    </form>
</body>
               
</html>

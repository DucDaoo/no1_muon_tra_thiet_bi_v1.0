<?php
    // session_start(); 
    require_once "../../app/controller/common.php"; // điều kiện nếu chưa login thì xuất ra màn hình login
    define('WEB_PATH', '../../web/avatar/tmp/'); 
    $name = $_SESSION['name'];
    $description = $_SESSION['description'];
    $avatar = $_SESSION['avatar'];
            
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>confirm</title>
    <link rel="stylesheet" href="../../web/css/device_add.css">
</head>
<body>
<form action="../controller/device_add.php" method="POST" enctype="multipart/form-data" class="form-device">
        <div class="row"> 
            <div class="col col-1"> 
                <label for="" class="label-name">Tên thiết bị</label>
            </div>
            <div class="col col-2"> 
                <input type="text" name="" id="" class="input-name" value="<?php echo $name; ?>"> 
            </div>
        </div>

        <div class="row">
            <div class="col col-1"> 
                <label for="" class="label-description">Mô tả chi tiết</label>
            </div>
            <div class="col col-2"> 
                <textarea name="" id="" cols="30" rows="10" class="textarea-description"><?php echo $description; ?></textarea> 
            </div>
        </div>

        <div class="row">
            <div class="col col-1"> 
                <label for="" class="label-avt">Avatar</label>
            </div>
            <img src="<?= WEB_PATH.$avatar ?>" alt="" width="200" height="200">
        </div>

        <div class="row">
            <div class="col col-half" style="text-align: center;">
                <input type="submit" name = "SuaLai" value="Sửa lại" class="input-btn">
            </div>
            <div class="col col-half" style="text-align: center;">
                <input type="submit" name = "DangKy" value="Đăng ký" class="input-btn">
            </div>
        </div>
    </form>
</body>
</html>
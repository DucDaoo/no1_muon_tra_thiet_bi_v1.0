<?php
require_once "../common/db.php";
require_once '../model/teacher.php';
require_once "../../app/controller/teacher_edit.php";
require_once "../../app/controller/common.php";
require_once "../common/define.php";

if (isset($_GET['teacherId']) ){
    $id_teacher =  $_GET['teacherId'];}
if (!isset($_GET['teacherId']) ){
    if (isset($_SESSION["id_teacher"]) ){
        $id_teacher=$_SESSION["id_teacher"];
    }
}
?>

<html>
    <head>
        <title>Sửa thông tin giáo viên</title>
        <link rel="stylesheet" href="../../web/css/teacher_edit_input.css">
    </head>

    <body>
        <?php
        $teacher = get_teacher_update($id_teacher);
        foreach ($teacher as $row) {
            $_SESSION["id_teacher"]=$id_teacher;
            if (empty($_SESSION["name"])) {
                $_SESSION["name"] = $row["name"];
            }

            if (empty($_SESSION["specialized"])) {
                $_SESSION["specialized"] = $row["specialized"];
            }

            if (empty($_SESSION["degree"])) {
                $_SESSION["degree"] = $row["degree"];
            }

            if (empty($_SESSION["avatar"])) {
                $_SESSION["avatar"] = $row["avatar"];
            }
            if (empty($_SESSION["description"])) {
                $_SESSION["description"] = $row["description"];
            }
        }
    ?>

        <form class='body' method='POST' action='' enctype="multipart/form-data">
            <div class='break'></div>
                <?php if (isset($err_name)) { ?>
                    <p class='error'><?php echo $err_name; ?> </p>
                <?php } ?>
            <div class='left'>
                <label class='title'>Họ và tên</label>
                <label class='break2'></label>
                <input class='input' type='textbox' name='name' value="<?php echo $_SESSION["name"]; ?>" type='text'>
            </div>

            </div>
            <div class='break'></div>
                <?php if (isset($err_specialized)) { ?>
                    <p class='error'> <?php echo $err_specialized;?> </p>
                <?php } ?>

            <div class='left'>
                <label class='title'>Chuyên ngành</label>
                <label class='break2'></label>
                <select class='input2' name='specialized' id='specialized'>
                    <?php foreach (_SPECIALIZED as $key => $value) {
                        if ($_SESSION["specialized"] == $key) {
                            $_SESSION["specialized"] = $value;
                        }
                    } ?>
                    <option value="<?php echo $_SESSION["specialized"]; ?>">
                    <?php echo $_SESSION["specialized"]; ?></option>
                    <?php foreach (_SPECIALIZED as $key => $value) { ?>
                    <option class='select' value='<?php echo $value; ?>'> <?php echo $value; ?> </option>
                    <?php } ?>
                </select>
            </div>

            <div class='break'></div>
                <?php if (isset($err_degree)) { ?>
                    <p class='error'> <?php echo $err_degree; ?> </p>
                <?php } ?>
            <div class='left'>
                <label class='title'>Học vị</label>
                <label class='break2'></label>
                <select class='input2' name='degree' id='degree'>
                    <?php foreach (_DEGREES as $key => $value) {
                        if ($_SESSION["degree"] == $key) {
                            $_SESSION["degree"] = $value;
                        }
                    } ?>
                    <option value="<?php echo $_SESSION[
                        "degree"
                    ]; ?>"><?php echo $_SESSION["degree"]; ?>
                </option>
                    <?php foreach (_DEGREES as $key => $value) { ?>
                    <option class='select' value='<?php echo $value; ?>'> <?php echo $value; ?> </option>
                    <?php } ?>
                </select>
            </div>

            <div class='break'></div>
            <div class="left">
                <label for="avatar" class="title">Avatar</label>
                <label class='break2'></label>
                <img id="img_avatar" src="<?php 
                    echo "../../web/avatar/".$_SESSION["avatar"]; 
                ?>">
            </div>

            <div class='break3'></div>
                <?php if (isset($err_avatar)) { ?>
                    <p class='error'> <?php echo $err_avatar; ?> </p>
                <?php } ?>
            <div class="left">
                <label class="title_hidden">Avatar</label>
                <input id="avatar_text" class="input2" type="text" name="avatar_text" value="<?php echo $_SESSION["avatar"]; ?>" readonly>
            </input>
                <input style="display:none;" class="input2" type="text" name="old_avatar" value="<?php echo $_SESSION["avatar"]; ?>" readonly>
            </input>
                <input type="file" id="avatar_file" name="avatar" title="browse" value="upload" accept="image/*" onchange="getFileData(this);" style="display:none;" />
                <label for="avatar_file" name="avatar" class="avatar_file">Browse</label>
            </div>

            <div class='break'></div>
                <?php if (isset($err_description)) { ?>
                    <p class='error'> <?php echo $err_description;?> </p>
                <?php } ?>
            <div class="left">
                <label class="title" for="description">Mô tả thêm</label>
                <label class='break2'></label>
                <textarea class="textarea" type="textarea" name="description" rows="3" cols="40" value=""><?php echo $_SESSION['description']?></textarea>
            </div>
            
            <div class='break3'></div>
            <input class='signup' type='submit' name='submit' value='Xác nhận'>
            <div class=' break'></div>
        </form>
    </body>
</html>

<script>
function getFileData(myFile) {
    var file = myFile.files[0];
    if (file) {
        var filename = file.name;
        document.getElementById ('img_avatar').src = URL.createObjectURL(file);
        document.getElementById('avatar_text').value = filename;
    }
}
</script> 

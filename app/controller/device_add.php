<?php
    require_once "../model/device.php";
    require_once "../view/device_add_confirm.php";
    $checked = false;
    $uploads_dir = __DIR__ . '/../../web/avatar/';
    $delete_dir = __DIR__ . '/../../web/avatar/tmp/';
    if($_SERVER['REQUEST_METHOD'] == "POST"){
        $name = $_SESSION['name'];
        $description = $_SESSION['description'];    
        $avatar = $_SESSION['avatar'];
        if (isset($_POST['DangKy'])) {
            $checked = save($name, $avatar, $description);
            if ($checked) {
                $filePath = $delete_dir.$avatar; 
                $destinationFilePath = $uploads_dir.$avatar; 
                if(rename($filePath, $destinationFilePath) ) {  
                    header("Location: ../view/device_add_complete.php");
                }  
            }
        }
        if (isset($_POST['SuaLai'])) {
            header("Location: ../view/device_add_input.php");
        }

    }
?>
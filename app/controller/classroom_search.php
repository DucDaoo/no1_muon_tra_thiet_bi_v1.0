<?php
session_start();
require_once '../model/classroom.php';
$_SESSION['building_id'] = $_GET['building_id'];
$_SESSION['keyword_name'] = $_GET['keyword_name'];
$_SESSION['classrooms'] = getClassRoomBy($_SESSION['building_id'], $_GET['keyword_name']);
header("Location: ../view/classroom_search.php");
?>
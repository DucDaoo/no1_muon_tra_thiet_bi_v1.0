<?php
ob_start();
require_once '../common/define.php';
require_once "../../app/controller/common.php";
require_once '../model/teacher.php';
require_once '../view/teacher_edit_input.php';
$err =0;
$err_id="";
$err_description="";
$err_avatar="";
$err_name = "";
$err_specialized = "";


if (isset($_POST['submit'])){

    $name = $_POST["name"] ??'';
    $specialized = $_POST["specialized"];
    $degree = $_POST["degree"];
    $id= $_SESSION["id_teacher"];
    $description=$_POST["description"];


    if(empty($name)){
        $err_name = "Hãy nhập tên giáo viên";
        $err = $err+1;
    } 
    if(strlen($name)>100){
        $err_name = 'Không nhập quá 100 kí tự';
        $err = $err+1;
    }

    if (empty($specialized)) {
        $err_specialized = 'Hãy chọn chuyên ngành';
        $err = $err+1;
    } 

    if (empty($degree)) {
        $err_degree = 'Hãy chọn bằng cấp';
        $err = $err+1;
    } 

    if (empty($description)) {
        $err_description = 'Hãy nhập mô tả chi tiết';
        $err = $err+1;
    } 

    if(strlen($description)>1000){
        $err_description = 'Không nhập quá 1000 kí tự';
    }

    if(empty($_SESSION["avatar"])){
        $err_avatar= 'Hãy chọn avatar';
        $err = $err+1;
    }

    if(empty($_POST["avatar"]) && empty($_POST["old_avatar"])){
        $err_avatar= 'Hãy chọn avatar';
        $err= $err+1;
    }

    if(empty($_FILES['avatar']['name'])){
        $_SESSION['avatar']= $_POST["old_avatar"];

    }else if (!empty($_FILES['avatar'])){
        if ($_FILES['avatar']['error'] > 0){
            $err=$err+1;
            echo $_FILES['avatar']['error'];
        }else{
            $id=$_SESSION["id_teacher"];
            $_SESSION['avatar']= $_FILES['avatar']['name'];
            $dir='../../web/avatar/';
            if (!file_exists('../../web/avatar/')) {
                mkdir('../../web/avatar/', 0777, true);
            }
            move_uploaded_file($_FILES['avatar']['tmp_name'],$dir."/".$_FILES['avatar']['name']);
        }
    }else{
        $err=$err+1;
    }

    if ($err==0){
        $_SESSION["id_teacher"]=$id;
        $_SESSION['name'] = $name;
        $_SESSION['specialized'] = $specialized;
        $_SESSION['degree'] = $degree;
        $_SESSION['description'] = trim($description);
        header('location:../view/teacher_edit_confirm.php');
    } 

}

if (isset($_POST["buttonSubmit"])) {
    if ($_POST["buttonSubmit"] == "Sửa") {
        header("location: ../view/teacher_edit_input.php");
    }

    if ($_POST["buttonSubmit"] == "Đăng ký") {
        foreach (_DEGREES as $key => $value) {
            if ($_SESSION["degree"] == $value) {
                $degree = $key;
            }
        }

        foreach (_SPECIALIZED as $key => $value) {
            if ($_SESSION["specialized"] == $value) {
                $specialized = $key;
            }
        }
        $name = $_SESSION["name"];
        $avatar = $_SESSION["avatar"];
        $description = trim($_SESSION["description"]);
        $id = $_SESSION["id_teacher"];
        $sql = update($id, $name, $avatar, $description, $specialized, $degree);

        $files = glob('../../web/avatar/'); 
        foreach($files as $file){ 
            if(is_file($file)) {
                unlink($file); 
            }
        } 
        if ($sql) {
            header("location: ../view/teacher_edit_complete.php");
            unset($_SESSION["id_teacher"]);
            unset($_SESSION["name"]);
            unset($_SESSION["avatar"]);
            unset($_SESSION["specialized"]);
            unset($_SESSION["degree"]);
            unset($_SESSION["description"]);
        }
    }
}
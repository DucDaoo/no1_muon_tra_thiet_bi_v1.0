<?php
include('../model/admin.php');

$_error = array();
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['login'])) {
        if (empty($_POST['loginid'])) {
            $_error['loginid'] = 'Hãy nhập login id.';
        } elseif (strlen($_POST['loginid']) < 4) {
            $_error[] = 'Hãy nhập login id tối thiểu 4 ký tự.';
        } else {
            $user_name = $_POST['loginid'];
        }

        if (empty($_POST['password'])) {
            $_error['password'] = 'Hãy nhập mật khẩu.';
        } elseif (strlen($_POST['password']) < 6) {
            $_error[] = 'Hãy nhập password tối thiểu 6 ký tự.';
        } else {
            $password = $_POST['password'];
        }

        if (!empty($_POST['loginid']) && !empty($_POST['password'])) {
            $row = querryAccount($user_name);
            if (is_array($row)) {
                if ($row['password'] == md5($password)) {
                    $_SESSION['loginned'] = true;
                    $_SESSION['loginid'] = $_POST["loginid"];
                    header('location: home.php');
                } else {
                    $_error[] = 'Mật khẩu không đúng.';
                }
            } else {
                $_error[] = 'Tài khoản và mật khẩu không đúng.';
            }
        }
    }
    
    if (!empty($_error)) {
        foreach ($_error as $v) {
            echo $v . '</br>';
        }
    }
}
<?php
    require_once "../common/db.php";
    require_once "../model/teacher.php";
    error_reporting(0);

    $list_teacher = [];

    if ($_SERVER['REQUEST_METHOD'] == "GET") {
        if (isset($keyword) || isset($_GET['specialized'])) {
            if (isset($_GET['keyword'])) {
                $keyword = $_GET['keyword'];
            }
            if (isset($_GET['specialized'])) {
                $specialized = $_GET['specialized'];
            }
            $list_teacher = search_teacher($keyword, $specialized);
        }
    }

    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        $teacher_id = $_POST['del'];
    }
    if ($teacher_id != '') {
        $sql = "DELETE FROM teachers WHERE teachers.id = $teacher_id";
        $statement = $conn->prepare($sql);
        $statement->execute();
    }
    require_once "../view/teacher_search.php";
    
?>
<?php
require_once "../model/teacher.php";
require_once "../model/device.php";


$list_all_teacher = get_all_teacher();
$list_devices = [];
if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (isset($name_device) || isset($_POST['id_teacher'])) {
        if (isset($_POST['name_device'])) {
            $name_device = $_POST['name_device'];
            $name_device = rtrim($name_device); 
            $name_device =  preg_replace('/[^A-Za-z0-9 ]/', '', $name_device);
        }
        if (isset($_POST['id_teacher'])) {
            if ($_POST['id_teacher']== ""){
                $id_teacher = $_POST['id_teacher'];
            }
            else{
                $id_teacher = $_POST['id_teacher'] ? (string)(int)$_POST['id_teacher'] : false;
                if ($id_teacher == false){
                    echo "Không tồn tại giáo viên này mời bạn ";
                    $link_address = "./device_loan_history_controller.php";
                    echo "<a href='".$link_address."'>thử lại</a>";
                    die();
                }
            }
        }
        $list_devices = search_devices_loan_history($name_device, $id_teacher);
    }
}
require_once "../view/device_loan_history.php";

function formatDate($date)
{
    $date = date('Y-m-d h:i:s', strtotime($date));
    $new_date = date('h:i d/m/Y', strtotime($date));
    return $new_date;
}

<?php
require_once "../model/device.php";
require_once "../controller/common.php";
$list_devices = [];

// Prepare data
if (isset($_GET['keyword']) || isset($_POST['keyword']))
    $keyword = $_GET['keyword'] ?? $_POST['keyword'];
else
    $keyword = '';

if (isset($_GET['status']) || isset($_POST['status']))
    $status = $_GET['status'] ?? $_POST['status'];
else
    $status = '';

// Handle request
if ($_SERVER['REQUEST_METHOD'] === "GET") {
    $list_devices = search_devices($keyword, $status);
}

if ($_SERVER['REQUEST_METHOD'] === "POST") {
    if (isset($_POST['deleteDevideId'])) {
        delete_device_by_id($_POST['deleteDevideId']);
    }
    $list_devices = search_devices($keyword, $status);
}


// Helper
function getDeviceStatusKeyById($id)
{
    return get_device_status($id);
}

function getDeviceStatusText($statusKey)
{
    switch ($statusKey) {
        case "empty":
            return "Rỗng";
        case "ready":
            return "Đang rảnh";
        case "borrow":
            return "Đang mượn";
        default:
            return "";
    }
}

// Render view
require_once "../view/device_search.php";

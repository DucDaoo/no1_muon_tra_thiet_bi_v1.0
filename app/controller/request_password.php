<?php
include '../model/admin.php';
// session_start();

$request_succ = "";
$id_check = $msg_error='';
// kiểm tra id nhập vào
if(isset($_POST['btn_request'])){
    if (empty($_POST['login_id_request'])){
        $msg_error = 'Hãy nhập login id';
    } else {
        $id_check = $_POST['login_id_request'];
        if (strlen($id_check) < 4) {
            $msg_error = 'Hãy nhập login id tối thiểu 4 kí tự';
        } else{
            $row_check = check_by_id($id_check);
            if ($row_check == false){
                $msg_error= "Login id không tồn tại trong hệ thống";
            } 
        }
    }
}

// kiểm tra rồi gọi hàm yêu cầu đổi mật khẩu
require_once '../view/request_password.php';  
if(isset($_POST['btn_request'])){
    if (empty($msg_error)) {
        $_SESSION['login_id'] = $id_check;
        $request_succ = request_password($_SESSION['login_id']);
    
    if ($request_succ == true){
        unset($_SESSION['login_id']);
        echo "<script>
                alert('Gửi yêu cầu thành công, vui lòng đăng nhập lại sau vài giây'); 
                window.location.href='../view/login.php';
            </script>"; 
        
    }
}
}
?>

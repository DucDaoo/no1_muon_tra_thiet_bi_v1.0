<?php
require_once "../model/classroom.php";
require_once "../common/db.php";
if (isset($_POST['type'])) {
    $class_name = $_POST['class_name'] ?? '';
    $mota = $_POST['mota'] ?? '';
    $avatar = $_FILES['avatar'] ?? $_POST['avatar'] ?? '';
    $building_id = $_POST["building_id"];
    if (!($class_name && $mota && $avatar && $building_id)) {
        echo 'Thiếu dữ liệu đầu vào';
    } else {
        if ($_POST['type'] == 0) {
            move_uploaded_file($avatar['tmp_name'],'../../web/avatar/tmp/'.$avatar['name']);
            require_once "../view/classroom_add_confirm.php";
        } else if ($_POST['type'] == 1){
           $id= save_class($class_name, $building_id, $mota, $avatar);
            mkdir('../../web/avatar/' . $id );
            rename('../../web/avatar/tmp/' . $_POST['avatar'], '../../web/avatar/' . $id . '/' . $_POST['avatar']);
            header('location: ../view/classroom_add_complete.php'); 
        } else if ($_POST['type'] == 2){
            header('location: ../view/classroom_add_input.php');
        }
    }
}
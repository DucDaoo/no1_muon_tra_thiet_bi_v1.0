<?php
    require_once "../common/db.php";
    require_once "../model/teacher.php";
    $flag = false;
    $uploads_dir = __DIR__ . '/../../web/avatar/';
    $delete_dir = __DIR__ . '/../../web/avatar/teacher/';

    $name = $_SESSION['name'];
    $description = $_SESSION['description'];    
    $avatar = $_SESSION['avatar'];
    $degree = $_SESSION['degree'];
    $specialized = $_SESSION['specialized'];


    if(isset($_POST['submit'])) {
        $flag = save_teacher($name, $avatar, $description, $specialized, $degree);
        if ($flag) {
            $filePath = $delete_dir.$avatar; 
            $destinationFilePath = $uploads_dir.$avatar; 
            if(rename($filePath, $destinationFilePath) ){  
                header("Location: ../view/teacher_add_complete.php");
                unset($_SESSION["name"]);
                unset($_SESSION["avatar"]);
                unset($_SESSION["specialized"]);
                unset($_SESSION["degree"]);
                unset($_SESSION["description"]);
            }  
        }
    }

    if (isset($_POST['fix'])) {
        header("Location: ../view/teacher_add_input.php");
    }
    
?>
<?php
require_once "../model/transaction.php";


if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (isset($_POST['device_id']) && isset($_POST['teacher_id']) && $_POST['class_id'] && $_POST['start_at'] && $_POST['end_at'] && $_POST['start_time_at'] && $_POST['end_time_at']) {
        $device_id = $_POST['device_id'];
        $teacher_id = $_POST['teacher_id'];
        $class_id = $_POST['class_id'];
        $start_transaction = $_POST['start_at'] . ' ' . $_POST['start_time_at'].':00';
        $end_transaction = $_POST['end_at'] . ' ' . $_POST['end_time_at'].':00';
        $result = add_transaction($device_id, $teacher_id, $class_id, $start_transaction, $end_transaction);
        if ($result != 1) var_dump($result);
        header('location: ../view/home.php');
    }
}



<?php
include "../common/db.php";  
// model Admin phục vụ việc giao tiếp với db 
// kiểm tra id có tồn tại trong hệ thống

function check_by_id($login_id){
    global $conn;
    $sql_admin = "SELECT * FROM admins WHERE login_id=:login_id";
    $stmt_admin = $conn->prepare($sql_admin);
    $stmt_admin->bindParam(':login_id', $login_id, PDO::PARAM_STR);
    $stmt_admin->execute();
    if ($stmt_admin->rowCount() > 0) {
        return true;
    } else 
        return false;
}

// đặt lại token với những tài khoản yêu cầu cấp lại mật khẩu
function request_password($login_id){
    global $conn;
    $rq_token = microtime();
    $sql_rq = 'UPDATE admins SET reset_password_token=:time_new WHERE login_id =:login_id';
    $request = $conn->prepare($sql_rq);
    $request->bindParam(':time_new',$rq_token, PDO::PARAM_STR);
    $request->bindParam(':login_id',$login_id, PDO::PARAM_STR);
    $request->execute();
    if ($request){
        return true;
    } else 
        return false;
}

// lấy ra thông tin của những người muốn thay đổi mật khẩu
function get_info(){
    global $conn;
    $sql = 'SELECT * FROM admins WHERE reset_password_token != ""';
    $info = $conn->prepare($sql);
    $info->execute();
    $list_info = $info->fetchAll(PDO::FETCH_ASSOC);
    return $list_info;
}

function reset_password($login_id, $new_password){
    global $conn;
    // biến password thành mã md5
    $new_pass = md5($new_password);
    $sql_query = 'UPDATE admins SET password=:pw, reset_password_token =NULL WHERE login_id =:login_id';
    $db_update = $conn->prepare($sql_query);
    $db_update->bindParam(':pw',$new_pass , PDO::PARAM_STR);
    $db_update->bindParam(':login_id',$login_id, PDO::PARAM_STR);
    $db_update->execute();
    // kiểm tra update thành công hay chưa
    if ($db_update){
        return true;
    } else 
        return false;
}

function querryAccount(String $user_name)
{
    global $conn;
    $sql_admin = "SELECT * FROM admins WHERE login_id=? LIMIT 1";
    $statement_admin = $conn->prepare($sql_admin);
    $statement_admin->bindParam(1, $user_name, PDO::PARAM_STR);
    $statement_admin->execute();
    $data = $statement_admin->fetch(PDO::FETCH_ASSOC);
    return $data;
}

// Tìm kiếm lịch sử mượn thiết bị
function search_devices_loan_history($name_device, $id_teacher)
{

    global $conn;
    if ($name_device == "" && $id_teacher == "") {
        $sql = "SELECT  devices.name as devices_name,
                        device_transactions.start_transaction_plan as device_transactions_start_transaction_plan,
                        device_transactions.end_transaction_plan as device_transactions_end_transaction_plan,
                        device_transactions.returned_date as device_transactions_returned_date,
		                teachers.name as teachers_name
                FROM device_transactions,`devices`,`teachers`
                WHERE devices.id = device_transactions.device_id AND device_transactions.teacher_id = teachers.id ";
    } else if ($id_teacher == "") {
        $sql = "SELECT  devices.name as devices_name,
                        device_transactions.start_transaction_plan as device_transactions_start_transaction_plan,
                        device_transactions.end_transaction_plan as device_transactions_end_transaction_plan,
                        device_transactions.returned_date as device_transactions_returned_date,
		                teachers.name as teachers_name
                FROM device_transactions,`devices`,`teachers`
                WHERE devices.name LIKE '%" . $name_device . "%' AND devices.id = device_transactions.device_id AND device_transactions.teacher_id = teachers.id";
    } else if ($name_device == "") {
        $sql = "SELECT  devices.name as devices_name,
                        device_transactions.start_transaction_plan as device_transactions_start_transaction_plan,
                        device_transactions.end_transaction_plan as device_transactions_end_transaction_plan,
                        device_transactions.returned_date as device_transactions_returned_date,
		                teachers.name as teachers_name
                FROM device_transactions,`devices`,`teachers`
                WHERE teacher_id =" . $id_teacher . " AND devices.id = device_transactions.device_id AND device_transactions.teacher_id = teachers.id";
    } else {
        $sql = "SELECT  devices.name as devices_name,
                        device_transactions.start_transaction_plan as device_transactions_start_transaction_plan,
                        device_transactions.end_transaction_plan as device_transactions_end_transaction_plan,
                        device_transactions.returned_date as device_transactions_returned_date,
		                teachers.name as teachers_name
                FROM device_transactions,`devices`,`teachers`
                WHERE devices.name LIKE '%" . $name_device . "%' AND devices.id = device_transactions.device_id AND device_transactions.teacher_id = teachers.id AND teacher_id =" . $id_teacher . " AND devices.id = device_transactions.device_id AND device_transactions.teacher_id = teachers.id";
    }
    $statement = $conn->prepare($sql);
    $statement->execute();
    $list_devices = $statement->fetchAll(PDO::FETCH_ASSOC);
    return $list_devices;
}

?>
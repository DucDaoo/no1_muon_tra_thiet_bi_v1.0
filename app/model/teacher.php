<?php
include "../common/db.php";

// danh sách giáo viên
function get_all_teacher() 
{
    global $conn;
    $sql = "SELECT * FROM teachers";
    $statement = $conn->prepare($sql);
    $statement -> execute();
    $list_teacher = $statement -> fetchAll(PDO::FETCH_ASSOC);
    return $list_teacher;
}

function get_teacher_update($id){
    global $conn;
    $sql_teacher = "SELECT * FROM teachers WHERE id=$id";
    return $conn->query($sql_teacher);
}

function update($id,$name, $avatar, $description, $specialized, $degree){
    global $conn;
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    $date=date('Y-m-d H:i:s');
    $sql = "UPDATE teachers SET name='$name', description='$description', 
    specialized='$specialized', degree='$degree', avatar='$avatar', 
    updated='$date' WHERE id='$id'";
    if($conn->query($sql)){
        return true; 
    }else return false;
}

function search_teacher($keyword, $specialized) {
    global $conn;
    if ($specialized === "000") {
        $sql = "SELECT * FROM `teachers` WHERE teachers.name LIKE '%$keyword%' 
                OR teachers.description LIKE '%$keyword%' 
                OR teachers.degree LIKE '%$keyword%';";
    } else {
        $sql = "SELECT * FROM `teachers` WHERE (teachers.name LIKE '%$keyword%'
                OR teachers.description LIKE '%$keyword%'
                OR teachers.degree LIKE '%$keyword%') 
                AND teachers.specialized = '$specialized';";
    }

    $statement = $conn->prepare($sql);
    $statement->execute();
    $list_teacher = $statement->fetchAll(PDO::FETCH_ASSOC);
    return $list_teacher;
}

function save_teacher($name,$avatar, $description, $specialized, $degree){
    global $conn;
    $date = new DateTime('NOW', new DateTimeZone('Asia/Ho_Chi_Minh')); 
    $convert = $date->format('Y-m-d H:i:s'); 
    $query = "insert into teachers (name,avatar,description,specialized,degree,updated,created) values(:name,:avatar, :description, :specialized,:degree,:updated, :created)";
    $sth = $conn->prepare($query);
    $sth->execute([
      ':name' => $name,
      ':avatar' => $avatar,
      ':description' => $description,
      ':specialized' => $specialized,
      ':degree' => $degree,
      ':updated' => "$convert",
      ':created' => "$convert"
    ]);
    $sth->closeCursor();
    return true;
}

//  Lấy ra danh sách giáo viên
function get_teacher() 
{
    global $conn;
    $sql = "SELECT * FROM teachers";
    $statement = $conn->prepare($sql);
    // Tiến hành xử lý câu lệnh sql, đối với SELECT thì sử dụng prepare() -> excute()
    $statement -> execute();
    $list_teacher = $statement -> fetchAll(PDO::FETCH_ASSOC);
    // print_r ($list_teacher); // check value  
    return $list_teacher;
}

function get_teacher_version_old()
{
    global $conn;
    $sql = "SELECT * FROM teachers";
    $statement = $conn->prepare($sql);

    // Tiến hành xử lý câu lệnh sql, đối với SELECT thì sử dụng prepare() -> excute()
    $statement -> execute();
    $list_teacher = [];
    while ($a = $statement->fetch()) {
        $list_teacher[] = $a;
    }
    // print_r ($list_teacher); // check value
    return $list_teacher;
}

?>


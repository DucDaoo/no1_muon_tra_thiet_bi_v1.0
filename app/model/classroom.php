<?php

require_once '../common/db.php';
function get_classroom() 
{
    global $conn;
    $sql2= "SELECT * FROM classrooms";
    $classroom2 = $conn->prepare($sql2);
    $classroom2->execute();
    return $classroom2;
}
function  save_class($class_name, $building_id, $mota, $avatar)
{
    global $conn;
    $sql = "INSERT INTO classrooms VALUES(null, '$class_name', '$avatar', '$mota',  CONCAT('T',$building_id), now(), now())";
    $statement = $conn->prepare($sql);
    $statement->execute();
    $sql = "select max(id) from classrooms";
    $statement = $conn->prepare($sql);
    $statement->execute();
    return $statement->fetch()[0];
}

function getClassRoomBy($building = '', $keyword = '') {
    global $conn;
    $sql = "SELECT * FROM classrooms WHERE ";
    $condition = "1";
    if ($building) {
        $condition .= " AND building LIKE '$building' ";
    }
    if ($keyword) {
        $condition .= " AND (name LIKE '%$keyword%' OR description LIKE '%$keyword%') ";
    }
    $sql .= $condition;
    $sql .= " ORDER BY id DESC";
    $classrooms = $conn->query($sql);
    return $classrooms -> fetchAll();
}

function deleteClassRoom($id) {
    global $conn;
    $sql = "DELETE FROM `device_transactions` WHERE classroom_id = '$id' ";
    $stmt = $conn->query($sql);
    $sql = "DELETE FROM `classrooms` WHERE id = '$id' ";   
    $stmt = $conn->query($sql);
    
}

function get_classroom_version_old()
{
    global $conn;
    $sql = "SELECT * FROM classrooms";
    $statement = $conn->prepare($sql);

    // Tiến hành xử lý câu lệnh sql, đối với SELECT thì sử dụng prepare() -> excute()
    $statement -> execute();
    $list_classroom = [];
    while ($a = $statement->fetch()) {
        $list_classroom[] = $a;
    }
    // print_r ($list_classroom); // check value
    return $list_classroom;
}

?>


<?php
include "../common/db.php";

// lưu các thông tin về thiết bị 
function save($name, $avatar, $description){
    global $conn;
    $date = new DateTime('NOW', new DateTimeZone('Asia/Ho_Chi_Minh')); 
    $createTime = $date->format('Y-m-d H:i:s'); 
    $query = "INSERT into devices (name,avatar,description,updated,created) values(:name, :avatar, :description, :updated, :created)";
    $save = $conn ->prepare($query);
    $save->bindParam(':name',$name, PDO::PARAM_STR);
    $save->bindParam(':description',$description, PDO::PARAM_STR);
    $save->bindParam(':avatar',$avatar, PDO::PARAM_STR);
    $save->bindParam(':name',$name, PDO::PARAM_STR);
    $save->bindParam(':updated',$createTime, PDO::PARAM_STR);
    $save->bindParam(':created',$createTime, PDO::PARAM_STR);
    $save->execute();
    return true;
}

// Tìm kiếm lịch sử mượn thiết bị
function search_devices_loan_history($name_device, $id_teacher)
{

    global $conn;
    if ($name_device == "" && $id_teacher == "") {
        $sql = "SELECT  devices.name as devices_name,
                        device_transactions.start_transaction_plan as device_transactions_start_transaction_plan,
                        device_transactions.end_transaction_plan as device_transactions_end_transaction_plan,
                        device_transactions.returned_date as device_transactions_returned_date,
		                teachers.name as teachers_name
                FROM `device_transactions`,`devices`,`teachers`
                WHERE devices.id = device_transactions.device_id AND device_transactions.teacher_id = teachers.id ";
    } else if ($id_teacher == "") {
        $sql = "SELECT  devices.name as devices_name,
                        device_transactions.start_transaction_plan as device_transactions_start_transaction_plan,
                        device_transactions.end_transaction_plan as device_transactions_end_transaction_plan,
                        device_transactions.returned_date as device_transactions_returned_date,
		                teachers.name as teachers_name
                FROM `device_transactions`,`devices`,`teachers`
                WHERE devices.name LIKE '%" . $name_device . "%' AND devices.id = device_transactions.device_id AND device_transactions.teacher_id = teachers.id";
    } else if ($name_device == "") {
        $sql = "SELECT  devices.name as devices_name,
                        device_transactions.start_transaction_plan as device_transactions_start_transaction_plan,
                        device_transactions.end_transaction_plan as device_transactions_end_transaction_plan,
                        device_transactions.returned_date as device_transactions_returned_date,
		                teachers.name as teachers_name
                FROM `device_transactions`,`devices`,`teachers`
                WHERE teacher_id =" . $id_teacher . " AND devices.id = device_transactions.device_id AND device_transactions.teacher_id = teachers.id";
    } else {
        $sql = "SELECT  devices.name as devices_name,
                        device_transactions.start_transaction_plan as device_transactions_start_transaction_plan,
                        device_transactions.end_transaction_plan as device_transactions_end_transaction_plan,
                        device_transactions.returned_date as device_transactions_returned_date,
		                teachers.name as teachers_name
                FROM `device_transactions`,`devices`,`teachers`
                WHERE devices.name LIKE '%" . $name_device . "%' AND devices.id = device_transactions.device_id AND device_transactions.teacher_id = teachers.id AND teacher_id =" . $id_teacher . " AND devices.id = device_transactions.device_id AND device_transactions.teacher_id = teachers.id";
    }
    $statement = $conn->prepare($sql);
    $statement->execute();
    $list_devices = $statement->fetchAll(PDO::FETCH_ASSOC);
    return $list_devices;
}

function search_devices($keyword, $status)
{
    global $conn;
    if ($status === "empty") {
        $sql = "SELECT * 
            FROM `devices`
            WHERE devices.name LIKE \"%$keyword%\" AND devices.id NOT IN (
                SELECT device_transactions.device_id FROM device_transactions
            )";
    } else if ($status === "ready") {
        $sql = "SELECT * 
        FROM `device_transactions` JOIN `devices` ON device_transactions.device_id = devices.id
        WHERE devices.name LIKE \"%$keyword%\" AND devices.id NOT IN (
            SELECT device_transactions.device_id FROM device_transactions WHERE device_transactions.returned_date IS NULL
        ) GROUP BY devices.id";
    } else if ($status === "borrow") {

        $sql = "SELECT * 
        FROM `device_transactions` JOIN `devices` ON device_transactions.device_id = devices.id
        WHERE devices.name LIKE \"%$keyword%\" AND devices.id IN (
            SELECT device_transactions.device_id FROM device_transactions WHERE device_transactions.returned_date IS NULL
        ) GROUP BY devices.id";
    } else {
        $sql = "SELECT * FROM `devices` WHERE devices.name LIKE \"%$keyword%\" ORDER BY devices.id DESC";
    }

    // Excute SQL
    $statement = $conn->prepare($sql);
    // Prevent SQL injection
    $statement->execute();
    $list_devices = $statement->fetchAll(PDO::FETCH_ASSOC);

    return $list_devices;
}

function search_devices_borrow($device_name, $teacher_id)
{

    global $conn;
    if ($device_name == "" && $teacher_id == "") {
        $sql = "SELECT  devices.name as devices_name,
                        device_transactions.start_transaction_plan as device_transactions_start_transaction_plan,
                        device_transactions.end_transaction_plan as device_transactions_end_transaction_plan,
                        device_transactions.returned_date as device_transactions_returned_date,
		                teachers.name as teachers_name
                FROM `device_transactions`,`devices`,`teachers`
                WHERE devices.id = device_transactions.device_id AND device_transactions.teacher_id = teachers.id ";
    } else if ($teacher_id == "") {
        $sql = "SELECT  devices.name as devices_name,
                        device_transactions.start_transaction_plan as device_transactions_start_transaction_plan,
                        device_transactions.end_transaction_plan as device_transactions_end_transaction_plan,
                        device_transactions.returned_date as device_transactions_returned_date,
		                teachers.name as teachers_name
                FROM `device_transactions`,`devices`,`teachers`
                WHERE devices.name LIKE '%" . $device_name . "%' AND devices.id = device_transactions.device_id AND device_transactions.teacher_id = teachers.id";
    } else if ($device_name == "") {
        $sql = "SELECT  devices.name as devices_name,
                        device_transactions.start_transaction_plan as device_transactions_start_transaction_plan,
                        device_transactions.end_transaction_plan as device_transactions_end_transaction_plan,
                        device_transactions.returned_date as device_transactions_returned_date,
		                teachers.name as teachers_name
                FROM `device_transactions`,`devices`,`teachers`
                WHERE teacher_id =" . $teacher_id . " AND devices.id = device_transactions.device_id AND device_transactions.teacher_id = teachers.id";
    } else {
        $sql = "SELECT  devices.name as devices_name,
                        device_transactions.start_transaction_plan as device_transactions_start_transaction_plan,
                        device_transactions.end_transaction_plan as device_transactions_end_transaction_plan,
                        device_transactions.returned_date as device_transactions_returned_date,
		                teachers.name as teachers_name
                FROM `device_transactions`,`devices`,`teachers`
                WHERE devices.name LIKE '%" . $device_name . "%' AND devices.id = device_transactions.device_id AND device_transactions.teacher_id = teachers.id AND teacher_id =" . $teacher_id . " AND devices.id = device_transactions.device_id AND device_transactions.teacher_id = teachers.id";
    }
    // print($sql); //check sql
    $statement = $conn->prepare($sql);
    // Tiến hành xử lý câu lệnh sql, đối với SELECT thì sử dụng prepare() -> excute()
    $statement->execute();
    $list_devices = $statement->fetchAll(PDO::FETCH_ASSOC);
    // print_r ($list_devices); // check value
    return $list_devices;
}

// Lấy status theo id của device
function get_device_status($deviceId)
{
    global $conn;
    $getDevicesSql = "SELECT * FROM `device_transactions` JOIN `devices` ON devices.id = device_transactions.device_id
                        WHERE device_transactions.device_id = $deviceId";
    $statement = $conn->prepare($getDevicesSql);
    $statement->execute();
    $list_devices = $statement->fetchAll(PDO::FETCH_ASSOC);
    if (count($list_devices) === 0) {
        return "empty";
    } else {
        for ($i = 0; $i < count($list_devices); $i++) {
            if (is_null($list_devices[$i]['returned_date'])) {
                return "borrow";
            }
        }
    }
    return "ready";
}

// Xóa thiết bị theo id
function delete_device_by_id($id)
{
    global $conn;
    if (isset($id)) {
        $id = (int) $id;
        $sql = "DELETE FROM devices WHERE devices.id = $id";
        $statement = $conn->prepare($sql);
        // Excute SQL
        $statement->execute();
    }
}

function get_device_by_id($id)
{
    $id = (int) $id;
    global $conn;
    $sql = "SELECT * FROM devices WHERE devices.id = $id LIMIT 1";
    $statement = $conn->prepare($sql);
    // Excute SQL
    $statement->execute();
    return $statement->fetch();
}

function search_devices_transaction()
{

    global $conn;
    $sql = "SELECT * FROM device_transactions  WHERE device_transactions.id IN (SELECT MAX(id) AS id
                FROM device_transactions
                GROUP BY device_id)";
    return $sql;
}
function search_count_devices_transaction()
{

    global $conn;
    $count_sql="SELECT  COUNT(*) FROM device_transactions  WHERE id IN (SELECT MAX(id) AS id
    FROM device_transactions
    GROUP BY device_id)";
    return  $count_sql;
}
function search_devices_transactions($s, $search_teacher,$search_classroom)
{

    global $conn;
    $sql="SELECT * FROM device_transactions,devices,teachers,classrooms WHERE
            devices.name LIKE '%$s%' AND teachers.name LIKE '%$search_teacher%' AND classrooms.name LIKE '%$search_classroom%' 
            AND  device_transactions.device_id=devices.id
            AND  device_transactions.teacher_id=teachers.id
            AND  device_transactions.classroom_id=classrooms.id
            AND device_transactions.id IN (SELECT MAX(id) AS id
                 FROM device_transactions
                 GROUP BY device_id)
            ";
    return $sql;
}
function search_count_devices_transactions($s, $search_teacher,$search_classroom)
{

    global $conn;
    $count_sql="SELECT count(*)FROM device_transactions,devices,teachers,classrooms WHERE 
        devices.name LIKE '%$s%' AND teachers.name LIKE '%$search_teacher%' AND classrooms.name LIKE '%$search_classroom%' 
        AND  device_transactions.device_id=devices.id
        AND  device_transactions.teacher_id=teachers.id
        AND  device_transactions.classroom_id=classrooms.id
        AND device_transactions.id IN (SELECT MAX(id) AS id
                 FROM device_transactions
                 GROUP BY device_id)";
    return  $count_sql;
}
function returned_date_devices_transactions($a){
    try{
        global $conn;
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $date=date('Y-m-d H:i:s');
        $sqls = "UPDATE device_transactions SET returned_date='$date' WHERE id='$a'";    
        $conn->exec($sqls);
    } catch(PDOException $e){
        die("ERROR: Không thể thực thi truy $sqls. " . $e->getMessage());
    }
}

?>

-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2023 at 04:27 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6
CREATE DATABASE cuoiki;

USE cuoiki;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cuoiki`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) NOT NULL,
  `login_id` varchar(20) NOT NULL,
  `password` varchar(64) NOT NULL,
  `actived_flag` int(1) NOT NULL DEFAULT 1,
  `reset_password_token` varchar(100) NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `login_id`, `password`, `actived_flag`, `reset_password_token`, `updated`, `created`) VALUES
(1, 'admin', 'fcea920f7412b5da7be0cf42b8c93759', 1, 'ef6109e3ed7380d293f4441d4e4b0af1', '2023-01-14 09:14:50', '2023-01-14 09:14:50'),
(3, 'developer', '6d073d4e616f200307c0a6866e22d7ea', 1, '69dcdc43831490e427f2ea9162f759c2', '2023-01-14 10:02:46', '2023-01-14 10:02:46'),
(4, 'ducdao', '6fcab6c59177809c9bf3d44386fd057e', 1,'','2023-01-26 10:02:46', '2023-01-26 10:02:46');
-- --------------------------------------------------------
-- password 
-- admin: 1234567
-- developer: duongminhdong
-- ducdao: ducdao2001
--
--
--
-- Table structure for table `classrooms`
--

CREATE TABLE `classrooms` (
  `id` int(10) NOT NULL,
  `name` varchar(250) NOT NULL,
  `avatar` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `building` char(10) NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classrooms`
--

INSERT INTO `classrooms` (`id`, `name`, `avatar`, `description`, `building`, `updated`, `created`) VALUES
(1, 'Phòng thí nghiệm', 'thinghiemroom.jpg', 'Nơi nghiên cứu và thử nghiệm các các công nghệ mới', 'T4', '2023-01-15 09:21:50', '2021-01-15 09:21:50'),
(2, 'Phòng máy', 'machineroom.jpg', 'Nơi thực hành tin học trên máy tính', 'T2', '2023-01-15 09:32:50', '2023-01-15 09:32:50'),
(3, 'Phòng tin học', 'tinhoc.jpg', 'Nơi thực hành tin học', 'T5', '2023-01-15 10:32:50', '2023-01-15 10:32:50'),
(4, 'Phòng lab', 'labroom.png', 'Phòng lab nơi thí nghiệm', 'T1', '2023-01-27 15:53:51', '2023-01-27 15:53:51'),
(5, 'Phòng họp cán bộ', 'hoproom.png', 'Phòng họp là nơi để thảo luận', 'T1', '2023-01-27 15:56:44', '2023-01-27 15:56:44'),
(6, 'Phòng nghỉ', 'phongnghi.png', 'Đây là nơi các cán bộ được nghỉ giữa buổi khi xong việc', '', '2023-01-27 15:56:44', '2023-01-27 15:56:44'),
(7, 'Phòng chờ giảng', 'chogiang.png', 'Nơi giảng viên ngồi nghỉ sau', 'T5', '2023-01-27 16:08:00', '2023-01-27 16:08:00'),
(8, 'Phòng công đoàn', 'congdoan.png', 'Tổ chức sự kiện đoàn hội', 'T3', '2023-01-27 16:08:00', '2023-01-27 16:08:00'),
(9, 'Phòng hội nghị', 'hoinghi.png', 'Tổ chức các sự kiện lớn', 'T2', '2023-01-27 16:08:00', '2023-01-27 16:08:00'),
(10, 'Phòng ngoại giao', 'ngoaigiao.png', 'Đón tiếp các khách nước ngoài', '', '2023-01-27 16:08:00', '2023-01-27 16:08:00');

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` int(10) NOT NULL,
  `name` varchar(250) NOT NULL,
  `avatar` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id`, `name`, `avatar`, `description`, `updated`, `created`) VALUES
(1, 'Máy chiếu', 'loakeo.jpg', 'Máy chiếu kĩ thuật số', '2023-01-15 09:25:32', '2023-01-15 09:25:32'),
(2, 'Điều hoà', 'dieuhoa.jpg', 'Máy điều hoà không khí', '2023-01-15 09:32:50', '2023-01-15 09:32:50'),
(3, 'Loa', 'dungcuphatnhac.jpg', 'Dụng cụ phát âm thanh', '2023-01-15 09:34:50', '2023-01-15 09:34:50'),
(4, 'Trực thăng Apache', 'tivi.png', 'tiếp cận mục tiêu', '2023-01-27 16:00:46', '2023-01-27 16:00:46'),
(5, 'máy chiếu ', 'maychieu.png', 'Công cụ hiển thị hình ảnh lên bề mặt phẳng', '2023-01-27 16:00:46', '2023-01-27 16:00:46'),
(6, 'Xe tăng Abrams', 'congchuyendoi.png', 'Cổng chuyển đổi cho phép truy xuất dữ liệu và hiển thị', '2023-01-27 16:02:04', '2023-01-27 16:02:04'),
(7, 'Chuột máy tính', 'chuotmay.png', 'Dụng cụ để hỗ trợ máy tính ', '2023-01-27 16:03:22', '2023-01-27 16:03:22'),
(8, "Niko's desert eagle", 'banphim.png', 'Navi vo doi', '2023-01-27 16:04:01', '2023-01-27 16:04:01'),
(9, 'Ak-47', 'micro.png', 'micro truyền âm cho phép âm thanh to hơn khi nói', '2023-01-27 16:04:01', '2023-01-27 16:04:01'),
(10, 'Máy ảnh', 'mayanh.png', '', '2023-01-27 16:06:49', '2023-01-27 16:06:49'),
(11, 'Đầu đạn tomahawk', 'tomahawk.png', 'Máy ảnh ', '2023-01-27 16:06:49', '2023-01-27 16:06:49');

-- --------------------------------------------------------

--
-- Table structure for table `device_transactions`
--

CREATE TABLE `device_transactions` (
  `id` int(10) NOT NULL,
  `device_id` int(10) NOT NULL,
  `teacher_id` int(10) NOT NULL,
  `classroom_id` int(10) NOT NULL,
  `comment` text NOT NULL,
  `start_transaction_plan` datetime NOT NULL,
  `end_transaction_plan` datetime NOT NULL,
  `returned_date` datetime DEFAULT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `device_transactions`
--

INSERT INTO `device_transactions` (`id`, `device_id`, `teacher_id`, `classroom_id`, `comment`, `start_transaction_plan`, `end_transaction_plan`, `returned_date`, `updated`, `created`) VALUES
(1, 1, 1, 1, '', '2023-01-14 23:21:04', '2023-01-16 23:21:04', NULL, '2023-01-15 12:21:04', '2023-01-14 07:21:04'),
(2, 3, 3, 1, '', '2023-01-14 23:24:30', '2023-01-16 23:24:30', '2023-01-14 23:24:30', '2023-01-15 09:24:30', '2023-01-14 23:24:30'),
(3, 4, 4, 1, '', '2023-01-14 23:26:38', '2022-01-15 23:26:38', NULL, '2023-01-15 23:26:38', '2022-01-15 23:26:38'),
(4, 6, 4, 2, 'Hoàng mượn 2 tiết', '2023-01-27 16:20:46', '2023-01-27 16:20:46', '2023-01-27 16:20:46', '2023-01-27 16:20:46', '2023-01-27 16:20:46'),
(5, 7, 6, 2, 'Chung mượn đột xuất', '2023-01-27 16:21:36', '2023-01-27 16:21:36', '', '2023-01-27 16:21:36', '2023-01-27 16:21:36'),
(8, 8, 5, 6, 'hihi', '2023-01-27 16:24:04', '2023-01-27 16:24:04', '', '2023-01-27 16:24:04', '2023-01-27 16:24:04');
-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(10) NOT NULL,
  `name` varchar(250) NOT NULL,
  `avatar` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `specialized` char(10) NOT NULL,
  `degree` char(10) NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `name`, `avatar`, `description`, `specialized`, `degree`, `updated`, `created`) VALUES
(1, 'Xuân', 'xuan_avt.png', 'Phó giáo sư trẻ', '003', '004', '2023-01-16 10:25:44', '2023-01-15 03:21:53'),
(2, 'Tiến', 'tien_avt.png', 'Tiềm năng trở thành phó giáo sư', '001', '003', '2023-01-16 10:26:08', '2023-01-15 14:01:17'),
(3, 'Công', 'cong_avt.png', 'Người phát hiện thuật toán mới', '002', '005', '2023-01-16 10:25:25', '2023-01-15 20:04:15'),
(4, 'Hoàng', 'hoang_avt.png', 'Người nghiên cứu sinh học', '001', '003', '2023-01-27 16:13:33', '2023-01-27 16:13:33'),
(5, 'Tình', 'tinh_avt.png', 'Người nghiên cứu quan hệ công chúng', '002', '003', '2023-01-27 16:13:33', '2023-01-27 16:13:33'),
(6, 'Chung', 'chung_avt.png', 'Thạc sĩ nghiên cứu hóa học', '001', '005', '2023-01-27 16:16:18', '2023-01-27 16:16:18'),
(7, 'Quỳnh', 'quynh_avt.png', 'Giảng viên đại số học', '003', '002', '2023-01-27 16:16:18', '2023-01-27 16:16:18'),
(8, 'Trần', 'tran_avt.png', 'Giảng viên khoa học máy tính', '001', '003', '2023-01-27 16:18:18', '2023-01-27 16:18:18'),
(9, 'Dần', 'dan_avt.png', 'Giảng viên', '002', '005', '2023-01-27 16:18:18', '2023-01-27 16:18:18'),
(10, 'Ly', 'ly_avt.png', 'Tiến sĩ ngôn ngữ học tự nhiên', '001', '001', '2023-01-27 16:19:57', '2023-01-27 16:19:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login_id` (`login_id`);

--
-- Indexes for table `classrooms`
--
ALTER TABLE `classrooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `device_transactions`
--
ALTER TABLE `device_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `device_id` (`device_id`),
  ADD KEY `teacher_id` (`teacher_id`),
  ADD KEY `classroom_id` (`classroom_id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `classrooms`
--
ALTER TABLE `classrooms`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `device_transactions`
--
ALTER TABLE `device_transactions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `device_transactions`
--
ALTER TABLE `device_transactions`
  ADD CONSTRAINT `device_transactions_ibfk_1` FOREIGN KEY (`classroom_id`) REFERENCES `classrooms` (`id`),
  ADD CONSTRAINT `device_transactions_ibfk_2` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`),
  ADD CONSTRAINT `device_transactions_ibfk_3` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

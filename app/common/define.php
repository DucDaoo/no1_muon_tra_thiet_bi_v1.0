<?php
define('_BUILDING', array(
    '1' => 'T1',
    '2' => 'T2',
    '3' => 'T3',
    '4' => 'T4',
    '5' => 'T5'
));

define("_SPECIALIZED", array(
    "000" => "",
    '001' => 'Khoa học máy tính',
    '002' => 'Khoa học dữ liệu',
    '003' => 'Hải dương học'
));

// Define degree array
define("_DEGREES",array(
    "000" => "",
    '001' => 'Cử nhân',
    '002' => 'Thạc sĩ',
    '003' => 'Tiến sĩ',
    '004' => 'Phó giáo sư',
    '005' => 'Giáo sư'
));
?>
